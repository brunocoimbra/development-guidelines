<h1>Programming Guidelines</h1>

Here we describe some things to keep in mind when writing your code. If you think that some explanation is incomplete or needing a better example you can always ask the lead programmer about it.

Also in this document we describe our internal set of architectures allowed to be used in the codes we develop. Those architectures can be easily mixed together and with third-party assets so they should be used on all projects developed by us.

- [General](#general)
  - [Always add access modifiers explicitly](#always-add-access-modifiers-explicitly)
  - [Always initialize Unity-serializable fields](#always-initialize-unity-serializable-fields)
  - [Always leave class fields as private](#always-leave-class-fields-as-private)
  - [Always use `localPosition` and `localRotation` whenever is possible](#always-use-localposition-and-localrotation-whenever-is-possible)
  - [Consider prototyping on empty scenes](#consider-prototyping-on-empty-scenes)
  - [Consider adding `sealed` to a class which was not designed for inheritance](#consider-adding-sealed-to-a-class-which-was-not-designed-for-inheritance)
  - [Consider using `var` when the type is explicit](#consider-using-var-when-the-type-is-explicit)
  - [Avoid hard-coded values](#avoid-hard-coded-values)
  - [Avoid static members](#avoid-static-members)
  - [Avoid singletons](#avoid-singletons)
  - [Avoid subscribing to Unity events on inspector](#avoid-subscribing-to-unity-events-on-inspector)
  - [Avoid implicit dependency injections](#avoid-implicit-dependency-injections)
  - [Avoid using Unity tags](#avoid-using-unity-tags)
  - [Avoid `using static XXX;`](#avoid-using-static-xxx)
  - [Avoid `using XXX = YYY;`](#avoid-using-xxx--yyy)
  - [Never reformat a code without permission](#never-reformat-a-code-without-permission)
  - [Never the setter should have more access than the getter](#never-the-setter-should-have-more-access-than-the-getter)
  - [Never use a specific character to check new lines when reading a file](#never-use-a-specific-character-to-check-new-lines-when-reading-a-file)
- [Naming Conventions](#naming-conventions)
  - [Event](#event)
  - [Naming sheet](#naming-sheet)
  - [Always use english-readable names](#always-use-english-readable-names)
  - [Always favor readability over brevity](#always-favor-readability-over-brevity)
  - [Always use affirmative actions or questions to name bool variables and functions](#always-use-affirmative-actions-or-questions-to-name-bool-variables-and-functions)
  - [Consider ending the name of a class with the name of the base class](#consider-ending-the-name-of-a-class-with-the-name-of-the-base-class)
  - [Consider omitting the `To` word if it is neither at the start or at the end](#consider-omitting-the-to-word-if-it-is-neither-at-the-start-or-at-the-end)
  - [Avoid abbreviations](#avoid-abbreviations)
- [Script Organization](#script-organization)
  - [File layout](#file-layout)
  - [Accessibility order](#accessibility-order)
  - [Keywords order](#keywords-order)
  - [Methods order](#methods-order)
  - [Always add using directives outside the namespace](#always-add-using-directives-outside-the-namespace)
  - [Always declare only one variable per declaration statement](#always-declare-only-one-variable-per-declaration-statement)
  - [Avoid multiple types on the same file](#avoid-multiple-types-on-the-same-file)
  - [Avoid long lines of code](#avoid-long-lines-of-code)
  - [Avoid comments](#avoid-comments)
  - [Avoid to use different accessibility levels for serialized values](#avoid-to-use-different-accessibility-levels-for-serialized-values)
  - [Never use C# regions](#never-use-c-regions)
- [Spacing and Blank Lines](#spacing-and-blank-lines)
  - [Always put a blank line between different "code sections"](#always-put-a-blank-line-between-different-%22code-sections%22)
  - [Consider to put a blank line before a control flow statement](#consider-to-put-a-blank-line-before-a-control-flow-statement)
  - [Never should put more than 1 blank line at a time](#never-should-put-more-than-1-blank-line-at-a-time)
  - [Never use tabs, use 4 spaces instead](#never-use-tabs-use-4-spaces-instead)
  - [Never indent processor directives](#never-indent-processor-directives)
- [Braces and Expression Bodies](#braces-and-expression-bodies)
  - [Always put braces on a new line](#always-put-braces-on-a-new-line)
  - [Always put braces on code blocks](#always-put-braces-on-code-blocks)
  - [Always inline auto-properties](#always-inline-auto-properties)
  - [Consider using expression bodies for single-line properties](#consider-using-expression-bodies-for-single-line-properties)
  - [Consider grouping multiple `for`, `foreach`, `using` and `while` statements](#consider-grouping-multiple-for-foreach-using-and-while-statements)
  - [Never use expression bodies on constructors and methods](#never-use-expression-bodies-on-constructors-and-methods)
- [Assemblies and Namespaces](#assemblies-and-namespaces)
- [Object Oriented Event-Driven Architecture (EDA)](#object-oriented-event-driven-architecture-eda)
  - [Special naming](#special-naming)
  - [About](#about)
- [Unity Hybrid Entity Component System (ECS)](#unity-hybrid-entity-component-system-ecs)
  - [Special naming](#special-naming-1)
  - [About](#about-1)

---
# General

## Always add access modifiers explicitly
Without the modifier your field or method can be confused with a local variable or local function.

```csharp
// correct
private void DoSomething() {...}

// wrong
void DoSomething() {...}
```

## Always initialize Unity-serializable fields
You need to do that to prevent Unity from spam the warning 0649 in the console after re-compiling the scripts. This need to be done for any serializable public field and any private field with the `SerializeField` attribute on it.

This is only needed inside any `struct` or `class` with `SerializableAttribute` and in any class that inherits from Unity `Object` (like `MonoBehaviour` or `ScriptableObject`).

```c#
[Serializable]
public struct MySerializableStruct
{
    public int SomeValue = default;
    public float AnotherValue = default;

    [NonSerialized] public int SomeNonSerializedValue;
}

[Serializable]
public sealed class MyMonoBehaviour : MonoBehaviour
{
    [SerializeField] int _myPrivateValue = default;

    private int _myAnotherPrivateValue;
}
```

>For an extended read about that issue see [this](https://issuetracker.unity3d.com/issues/serializedfield-fields-produce-field-is-never-assigned-to-dot-dot-dot-warning).

## Always leave class fields as private
To enforce encapsulation we should always leave a class field as private. If you need to expose it at any level you can create a protected/internal/private property for it.

Structs can have public variables without any issue, as they are immutable already and are meant to represent values.

## Always use `localPosition` and `localRotation` whenever is possible
This allows to easily create objects hierarchies without breaking the functionality. If someone wants the object to move relative to the world it should do because the hierarchy was built with that in mind and not because some mysterious code is enforcing that.

If for some reason you need to enforce a world-relative translation and/or rotation then consider exposing an option for that at least.

## Consider prototyping on empty scenes
This will help you to make your code more flexible by not centralizing your logic into an specific situation or setup. Avoid to commit those prototyping scenes as they may bloat the source control unnecessarily.

## Consider adding `sealed` to a class which was not designed for inheritance
By default all classes are inheritable but not necessarily the class was designed to be inherited - specially a custom `MonoBehaviour` which we don't usually mark the magic methods as virtual, thus leading to issues in the execution. So we advice you to mark every class as sealed unless you planned the inheritance ahead.

## Consider using `var` when the type is explicit
Like with `new T()`, in the scoped variable of a `foreach` loop, `GetComponent<T>()`, `AddComponent<T>()`, local strings and local bools.

Do not use `var` for local ints, floats, bytes and other numeric types as it may cause confusion for other developer, neither for non-generic method returns like `var weapon = GetWeapon()`.

## Avoid hard-coded values
Create constants and readonly values whenever makes sense.

## Avoid static members
Static data - which include static properties and static events - tends to be hard to debug, close to impossible to multi-thread properly and create several limitations in your code.

## Avoid singletons
We do not ban the usage of singletons but we do know that most developers tends to use them by convenience - which tends to evolve into a big spaghetti code where every objects have access to every other object state.

Because of that we kindly ask you to re-think if there isn't a better way to solve your issue other than using a singleton. If you still think this is the best road to follow then you should consult the lead programmer to confirm that.

## Avoid subscribing to Unity events on inspector
Things like Button, Toggle and InputField lets the developer to listen to events in the inspector.

The problem here is that this is really hard to debug (third party tools may help with that, but none really solves the problem) and things tends to break silently as more people gets involved into the project. So the best approach is to always listen to those events through code.

In case you find a real need to subscribe in the inspector you should talk with the lead programmer first to ensure that this is the best approach to solve your problem.

```c#
[SerializeField] private Button _button;

private void Start()
{
    _button.onClick.AddListener(HandleButtonClick);
}

private void HandleButtonClick()
{
    Debug.Log("Button click");
}
```

## Avoid implicit dependency injections
Frameworks like Zenject should not be used, use the object hierarchy itself instead to inject dependencies into your objects. It is easier to debug a code without "magic" happening under the hood.

We may not be able to avoid that when using third-party assets and that's okay.

## Avoid using Unity tags
We should use an `interface` or empty `MonoBehaviour` instead, which we can then query for it using a plain and simple `GetComponent<T>()`. If you need to query on a frame-basis then you should consider using [ECS] instead.

We may not be able to avoid that when using third-party assets and that's okay.

## Avoid `using static XXX;`
This will fill the intellisense with all the static stuff from that type so always consult the lead programmer about using it.

## Avoid `using XXX = YYY;`
This may confuse someone maintaining your code as it hides actual type being used.

A good usage for it is when using both `System` and `UnityEngine` namespaces at same time as you can then add `using Object = UnityEngine.Object;` to differ it from `System.Object` (which you will always want to access through the `object` keyword anyway).

## Never reformat a code without permission
If someone didn't follow the pattern correctly you should communicate that person about it and ask the lead programmer for permission to update the code format. Reformatting the code blindly can be dangerous as it may cause unnecessary merge conflicts.

## Never the setter should have more access than the getter
If the getter needs to be omitted for some reason or have a more restrict access level then change the setter to be a method instead.

```c#
public class Player
{
    private int _someValue;
    private int _anotherValue;

    protected int SomeValue => _someValue;

    public void SetSomeValue(int value)
    {
        _someValue = value;
    }

    public void SetAnotherValue(int value)
    {
        _anotherValue = value;
    }
}
```

## Never use a specific character to check new lines when reading a file
The file could have been written with another newline character so you should always check both `\r\n` (Windows-style) and `\n` (Unix-style) to check for new lines.

---
# Naming Conventions

## Event
[Event]:#event

First of all we need to present to you our definition of [Event]. When we talk about an [Event] in this document we are **not** talking about a specific kind of event - C# events, UnityEvents or other event implementation - but about a special naming rule that is used in any of those kinds described.

The [Event] we are talking about here are actions with a special meaningful name. An [Event] should tell who or what is sending it, why and when. If the sender is the context itself, then the sender should be omitted.

For example, a menu class can have an [Event] for when it is `Opened` or before a `ButtonClick`.

When the [Event] is gonna be sended can be described with a simple set of spelling rules:

- Use simple present to indicate that it is invoked early in the logic.
- Use simple past to indicate that it is invoked later in the logic.
- Use present continuous to indicate that it may be invoked multiple times and/or in an undefined moment during the logic.

For example, you could have an [Event] when the menu starts to `Load`, another one invoked each frame while it is `Loading` and finally another one when it is finally `Loaded`.

## Naming sheet

>**Note:** when we use `noun` in the format, we refer to both nouns and noun phrases.

| Identifiers | Casing | Formats | Examples | Observations |
| :--- | :--- | :--- | :--- | :--- |
| Class<br/> Struct<br/> Property<br/> Constants<br/> Readonly | PascalCase | {Noun} | `Player`<br/> `SpeedRange`<br/> `PlayerController`<br/> `DefaultSpeed`<br/> `InitialSpeed` | It should be clear which object or data it represents. |
| Abstract Class | PascalCase | {Noun}Base | `CharacterBase` | - |
| Tests Class | PascalCase | {Type}Tests | `PlayerTests`<br/>`PrefabUtilityTests` | In the provided examples it was expected to exist a type named `Player` and another one called `PrefabUtility`. |
| Extensions Class | PascalCase | {Type}Extensions | `TransformExtensions`<br/>`RigidbodyExtensions` | - |
| Delegates | PascalCase | {[Event]}`Handler` | `ButtonClickHandler` | - |
| Enums | PascalCase | {Noun} | `GameMode`<br/> `DisplayOptions` | Use singular for simple enums and plural for flag enums. |
| Interfaces | PascalCase | `I`{Adjective}<br/> `I`{[Event]}`Handler` | `IDamageable`<br/> `IDamageTakenHandler` | - |
| Generic arguments | PascalCase | `T`<br/> `T`{Noun}<br/> | `List<T>`<br/> `Dictionary<TKey, TValue>` | If there is only one argument, favor using just `T` as the name. |
| C# event<br/> Method as event handler | PascalCase | `On`{[Event]} | `OnButtonClick`<br/> `OnButtonClick()` | There are cases where a method can have the same behaviour of an C# event but without the overhead. In those cases we can use the same naming convention to highlight that. |
| Method as event listener | PascalCase | `Handle`{[Event]} | `HandleButtonClick()` | Those methods are directly tied to the execution of an event. We use them to avoid anonymous methods as much as possible. |
| Method | PascalCase | {Action} | `Jump()` | - |
| Async methods | PascalCase | {Action}`Async` | `DownloadFileAsync()` | - |
| Coroutine methods | PascalCase | {Action}`Coroutine` | `DownloadFileCoroutine()` | - |
| Public fields<br/>Constant fields<br/>Readonly fields | PascalCase | {Noun} | `SpeedRange` | Only structs can have non-private fields that are neither constant or readonly. |
| Property backing field | PascalCase | `m_`{PropertyName}<br/> `s_`{PropertyName} | `m_SpeedRange`<br/> `s_SpeedRange` | Use `m_` for instance properties and `s_` for static properties. |
| Private fields | camelCase | `_`{noun} | `_speedRange` | - |
| Parameters<br/> Local variables | camelCase | {noun} | `speedRange` | - |
| Local functions | camelCase | {action}<br/> | `debugElement()` | - |

>For an extended read about how to handle capitalization see [this](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/capitalization-conventions#capitalizing-compound-words-and-common-terms).

## Always use english-readable names
`AdvancedOptionsButton` is more readable than `ButtonOptionsAdvanced`.

## Always favor readability over brevity
`verticalSpeed` is better than `ySpeed`.

## Always use affirmative actions or questions to name bool variables and functions
`CanPlay()`, `isPlaying`, `_wasPlayed`, `HasPlayer()` and `play` are good examples.

## Consider ending the name of a class with the name of the base class
Exceptions applies to classes that inherits from the Unity's API classes. When inheriting from an abstract class, omit the `Base` suffix.

## Consider omitting the `To` word if it is neither at the start or at the end
`StringToIntDictionary` can be `StringIntDictionary`.<br/>
`TryToPlay` can be `TryPlay`.<br/>
`ToAnimationEvent` or `MoveTo` can lose its true meaning if `To` is omitted.

## Avoid abbreviations
It may be clear for the creator of the code but for someone else it may not. Exceptions apply for some well-known abbreviations like `min`, `max`, `vr`, `ar`, `ui`, `id`, `e` for the try-catch exception and `i` for the for-loop scoped variable (`j` is also allowed for a nested for-loop, but usually you will want a more descriptive name when nesting for-loops). In case of doubt you can always ask the lead programmer.

---
# Script Organization

## File layout
In order to make easier to read all files across the same project we separate each code in different sections where each section have special ordering rules. This way we can quickly distinguish where we are in the file without the help of specific tools (like a file mini-map) or the c# regions (as having a pre-defined layout makes them redundant).

The table below shows the exact order that those sections should appear.

| Section Name | Ordering Rule | Blank Line Between Members |
| :--- | :--- | :--- |
| Using directives | using XXX; (Alphabetically)<br/>using static XXX;<br/>using XXX = YYY; | No |
| Namespace | - | - |
| Nested types | By type (`delegate` then `enum` then `struct` then `class`) | Yes |
| Fields<br/>Events | By [keywords] (but [keep your serialized fields together]) | No if same [keywords] |
| Static constructor | - | - |
| Instance constructors | By [accessibility] | Yes |
| Destructor | - | - |
| Event accessors | By [keywords] | No if same [keywords] |
| Indexers<br/>Properties | `get-only` (ordered by [keywords])<br/>`get & set` (ordered by [keywords]) | No if same [keywords] |
| Methods | See [methods] | Yes |

## Accessibility order
[accessibility]:#accessibility-order

The more exposed the member it, higher its priority.

- `public`
- `internal`
- `protected internal`
- `protected`
- `private protected`
- `private`

## Keywords order
[keywords]:#keywords-order

All keywords should be sorted in a specific order. This improves the code readability and avoid errors due misunderstanding of the code.

The order below is for both when ordering the members with the keywords and for ordering the keywords between themselves.

- [accessibility]
- `unsafe`
- `const`
- `static`
- `readonly`
- `volatile`
- `event`
- `extern`
- `abstract`
- `virtual`
- `sealed`
- `override`
- `partial`
- `async`

Example keyword ordering:
```c#
// correct
public unsafe sealed override async void DoSomething() {...}

// wrong
unsafe async override sealed public void DoSomething() {...}
```

Example members ordering:
```c#
// correct
public abstract async void DoSomethingAbstractAsync();

public abstract void DoSomethingAbstract();

public async void DoSomethingAsync() {...}

public void DoSomething() {...}

void IInterface.SomeExplicitMethod() {...}

// wrong
void IInterface.SomeExplicitMethod() {...}

public async void DoSomethingAsync() {...}

public void DoSomething() {...}

public abstract void DoSomethingAbstract();

public abstract async void DoSomethingAbstractAsync();
```

## Methods order
[methods]:#methods-order

Methods ordering should be firstly based on the [keywords]. Between methods with same [keywords] you should then follow this list:

- Unity callbacks
  - `Reset` then `OnValidate`
  - `Awake` then `OnEnable` then `Start`
  - `OnDisable` then `OnDestroy`
  - `FixedUpdate` then `Update` then `LateUpdate`
  - `OnTriggerXXX` (`Enter` then `Stay` then `Exit`)
  - `OnCollisionXXX` (`Enter` then `Stay` then `Exit`)
  - Any other Unity callback
  - `OnDrawGizmos`
- Methods with Unity attributes
  - Like `MenuItem` and `RuntimeInitializeOnLoadMethod`
- Other methods

Example methods ordering:
```c#
public void ExecuteSomething() {...}

protected void OnTriggerEnter(Collider collider) {...}

[Preserve, RuntimeInitializeOnLoadMethod]
private static void InitializeOnLoad() {...}

private static void ExecuteSomethingStatic() {...}

private async void ExecuteSomethingAsync() {...}

private void Awake() {...}

private void Start() {...}

private void Update() {...}

[Conditional("UNITY_EDITOR")]
private void Print(string message) {...}

private void ExecuteNonUnityCallback() {...}

private void ExecuteSomethingElse() {...}

void IDamageable.TakeDamage(float amount) {...}
```

## Always add using directives outside the namespace
This way the compiler will always prioritize the namespace hierarchy and name conflicts with common namespaces will be quickly detected.

## Always declare only one variable per declaration statement
Here we have two different cases:

1. Single-line multi-declaration statements

```c#
float speed, speedMultiplier, positionX, positionY, positionZ;
```

2. Multi-line multi-declaration statements

```c#
float number,
      speedMultiplier,
      positionX,
      positionY,
      positionZ;
```

In the first case, just adding, removing or renaming a variable would change the entire line.

In the second case removing the last line or adding a new variable after the last one would always result in a change on the one above it (to move the `;` around).

Those thing bloats the version control system and can cause unnecessary merge conflicts so we should always split the declarations between multiple statements and group related elements in a class or a struct if that makes sense:

```c#
public struct Position
{
    public float X;
    public float Y;
    public float Z;
}

float speed;
float speedMultiplier;
Position position;
```

## Avoid multiple types on the same file
Exceptions apply for nested types but be careful to not bloat the code with many nested types on the same class or struct. If you have a huge amount of nested types due some big internal logic you may want to consider moving all that stuff into a different namespace and/or assembly.

## Avoid long lines of code
Long lines of code are mostly seem in method calls which parameters are result from another method. In those cases you can save the result in a local variable and use that as a parameter for following method. This greatly improves readability, reduces merge conflicts and makes easier to debug properly.

Another common case are nested ifs, for-loops, for-each and switch-cases that could return earlier or be split into functions.

```c#
// preferable
private void Awake()
{
    GameObject spawnPoint = LevelSystem.GetSpawnPointForObject(gameObject);
    transform.position = spawnPoint.transform.position;
}

public void TakeDamage(float amount)
{
    if (health == 0)
    {
        return;
    }

    health = Mathf.Max(health - amount, 0);

    if (health == 0)
    {
        InvokeDieEvent();
    }
}

private void InvokeDieEvent()
{
    foreach (var handler in DieEvent)
    {
        handler.Invoke(this);
    }
}

// instead of
private void Awake()
{
    transform.position = LevelSystem.GetSpawnPointForObject(gameObject).transform.position;
}

public void TakeDamage(float amount)
{
    if (health > 0)
    {
        health = Mathf.Max(health - amount, 0);

        if (health == 0)
        {
            foreach (var listener in DieEvent)
            {
                listener.Invoke(this);
            }
        }
    }
}
```

## Avoid comments
We should always prefer auto-documented code, like putting more descriptive names on the members and splitting more the functions and classes into smaller ones. However, you can (and should) add comments if you really think that it is necessary for some reason, like when you are doing some temporary hack or leaving an implementation incomplete for some reason, just take care to ensure that you are doing this for the right reason. In case of doubt you can always consult the lead programmer.

Summaries for public and protected APIs are always welcome as they help others to quickly understand the API without looking into the internal code logic.

## Avoid to use different accessibility levels for serialized values
[keep your serialized fields together]:#avoid-use-different-accessibility-for-serialized-values

This helps to keep all inspector fields together, which helps a lot when debugging inspector values.

```c#
// preferable
public struct PlayerData
{
    private const float MinHealth = 0;

    [SerializeField] private string m_Name;
    [SerializeField] private float m_CurrentHealth;
    [SerializeField] private float m_MaxHealth;

    public float MaxHealth => m_MaxHealth;
    public float CurrentHealth
    {
        get => m_CurrentHealth;
        set => m_CurrentHealth = value;
    }
    public string Name
    {
        get => m_Name;
        set => m_Name = value;
    }
}

// instead of
public struct PlayerData
{
    public string Name;
    public float CurrentHealth;

    private const float MinHealth = 0;

    [SerializeField] private float m_MaxHealth;

    public float MaxHealth => m_MaxHealth;
}
```

## Never use C# regions
If you think that your code would be easier to read with them then you should think about breaking your class into smaller ones. This way your code will look cleaner and follow the single-responsibility principle.

```c#
// correct
public class PlayerPhysics
{
    public void Move(Vector2 axis)
    {
        // ...
    }

    public void Jump()
    {
        // ...
    }
}

public class PlayerWeapon
{
    public void Shoot(Vector3 direction)
    {
        // ...
    }

    public void Reload()
    {
        // ...
    }
}

// wrong
public class Player
{
    #region Physics Methods

    public void Move(Vector2 axis)
    {
        // ...
    }

    public void Jump()
    {
        // ...
    }

    #endregion

    #region Weapon Methods

    public void Shoot(Vector3 direction)
    {
        // ...
    }

    public void Reload()
    {
        // ...
    }

    #endregion
}
```

---
# Spacing and Blank Lines

## Always put a blank line between different "code sections"
Like between any `c# event` and a `field` even if there is no blank line between the events itself.

```c#
public event Action OnLoad;
public event Action OnLoaded;

private float _someValue;
private float _anotherValue;

public void DoSomething() {...}
```

## Consider to put a blank line before a control flow statement
Control flow statements are anything that may change the course of execution of a method (`if`, `switch`, `case`, `return`, etc). The only exceptions being:

1. Before the `else`, which should be in the line directly after the closing `}` of the `if`.
2. On the `while` of the `do` statement, which should be directly after the `}` on the same line (use 1 space between like `"} while (true);"`).

That improves the visibility of those important statements and should be ignored if the statement is the first thing after an `{`.

## Never should put more than 1 blank line at a time
It's okay to separate stuff inside the methods with a blank line but don't ever use more than 1 blank line at once.

## Never use tabs, use 4 spaces instead
This is the default behaviour for the most common C# IDEs and using a different behavior than the rest of the team can cause unnecessary merge conflicts.

## Never indent processor directives
Processor directives (`#if`, `#define`, `#pragma`) should never be indented, put them always on the leftmost side.

---
# Braces and Expression Bodies

## Always put braces on a new line
We are just following the standard c# guideline to close the doors to any further discussion about that topic as there is no right or wrong here.

## Always put braces on code blocks
You should always add braces on any statement like `if`, `else`, `switch`, `case` and `do`.

```c#
public void Example(StuffMode stuff)
{
    if (_condition)
    {
        switch (stuff)
        {
            case StuffMode.Something:
            {
                // Do something

                break;
            }

            default:
            {
                break;
            }
        }
    }
    else
    {
        // Do another thing
    }
}
```

## Always inline auto-properties
This helps to quickly differ auto-properties from other properties.

```c#
public int MyReadonlyProperty { get; } = 100;
public int MyProperty { get; set; }
```

## Consider using expression bodies for single-line properties
Most properties are just an encapsulation to some backing field and aren't meant to have its logic expanded. In those cases you may decide to use expression bodies instead of the standard block bodies.

```c#
public int MyValue => m_MyValue;
public float MyClampedValue
{
    get => m_MyClampedValue;
    set => m_MyClampedValue = Mathf.Clamp(value, MinValue, MaxValue);
}
public float MyComplexValue
{
    get => m_MyComplexValue;
    set
    {
        if (MyValue > MyClampedValue)
        {
            m_MyComplexValue = value;
        }
    }
}
```

## Consider grouping multiple `for`, `foreach`, `using` and `while` statements
You may consider grouping multiple statements of same kind when both share the exact same logic.

```c#
foreach (int x in Lines)
foreach (int y in Columns)
{
    using (var xReader = new StreamReader($"{x}.txt"))
    using (var yReader = new StreamReader($"{y}.txt"))
    {
        string xLine = xReader.ReadLine();
        string yLine = yReader.ReadLine();

        while (xLine != null && yLine != null) 
        {
            print($"{xLine} - {yLine}");

            xLine = xReader.ReadLine();
            yLine = yReader.ReadLine();
        }
    }
}

```

## Never use expression bodies on constructors and methods
They can be easily confused with properties so always use the standard block bodies with them.
 
---
# Assemblies and Namespaces

  - Always create a root namespace for your project.
  - Always create a folder hierarchy that match your namespaces.
  - Always create assembly definition files for your project. It is expected to have at least the following folder structure in each project:
    ```
    Scripts
    '- Editor
       '- MyNamespaceEditor.asmdef
    '- Runtime
       '- MyNamespace.asmdef
    '- Tests
       '- Editor
          '- MyNamespaceEditorTests.asmdef
       '- Runtime
          '- MyNamespaceTests.asmdef
    ```
  - Avoid creating a namespace just for the Editor and/or Tests, use those words only to differentiate the assemblies and only on the root namespace. For example:
    ```c#
    // MyNamespace.asmdef
    namespace MyNamespace
    {
        public static class MyRuntimeClass {...}
    }

    // MyNamespaceTest.asmdef
    namespace MyNamespace
    {
        public static class MyRuntimeClassTests {...}
    }

    // MyNamespaceEditor.Stuff.asmdef
    namespace MyNamespace.Stuff
    {
        public static class MyEditorClass {...}
    }

    // MyNamespaceEditorTest.Stuff.asmdef
    namespace MyNamespace.Stuff
    {
        public static class MyEditorClassTests {...}
    }
    ```

---
# Object Oriented Event-Driven Architecture (EDA)
[EDA]:#object-oriented-event-driven-architecture-eda

## Special naming
- Suffix `System` for any `MonoBehaviour` that is an entry for the Unity system, like a GameSystem and some types of Singletons. Those classes are meant to be the only ones with the special `Awake`, `Start`, `FixedUpdate`, `Update` and `LateUpdate` callbacks and should be attached to an empty `GameObject` in the scene root with the same name (one `GameObject` per `System`).
- Suffix `Manager` for any `MonoBehaviour` that should work as an extension for a `System`, like an EnemyManager to handle all the Enemy instances. Those classes are meant to be responsible by managing all instances of a specific type or a group of specific types. You can create methods like `OnAwake` and `OnUpdate` to be called by the `System` it belongs to.
- Suffix `Data` for a `ScriptableObject` meant to hold shared data. You can think of those as "constants" or "statics" for Prefabs and Prefab Variants.
- Suffix `View` for a `MonoBehaviour` used just for visual representation, such as for showing a health bar on the screen or for listening to animation events to forward those back to the logic container.
- Always follow the same naming on related objects (like PlayerManager, Player, PlayerView and PlayerData).

## About
First three special things to have in mind about our [EDA]:

- It is very tied to the hierarchy of control of the GameObjects in the scene. Every `System` should be as independent as possible from any other `System`, but a `Manager` should not rely on a specific `System`. In fact, classes deeper in the hierarchy should have no acknowledgement of the classes above it. 
- We encourage the creation of custom reusable components too. You could even make an entire `System` (usually you would use a singleton here) just to create a more complex custom component for the project.
- Use a custom `interface` to create interactions between two objects in different ramifications of the hierarchy - or even from different systems.

Having saying that, let me show you a pseudo-code for a MultiplayerSystem were each Player can interact with each other:

```c#
public interface IInteractable
{
    void Interact(GameObject source);
}

public sealed class MultiplayerSystem : MonoBehaviour
{
    [SerializeField] private InputManager _inputManager;
    [SerializeField] private PlayerManager _playerManager;

    private void Awake()
    {
        _inputManager.OnMoveAxisUpdated += PlayerManager.Move;
    }

    private void Update()
    {
        _inputManager.OnUpdate();
    }
}

public sealed class InputManager : MonoBehaviour
{
    public delegate void MoveAxisUpdatedHandler(int id, Vector2 axis);

    public event MoveAxisUpdatedHandler OnMoveAxisUpdated;

    // Calls OnMoveAxisUpdated?.Invoke(id, axis) every frame
    public void OnUpdate() {...}
}

public sealed class PlayerManager : MonoBehaviour
{
    [SerializeField] private Player[] _players;

    public void Move(int id, Vector2 axis)
    {
        _player[id].Move(axis);
    }
}

public sealed class Player : MonoBehaviour, IInteractable
{
    // Moves the player so that we can trigger the interaction
    public void Move(Vector2 axis) {...}

    private void OnTriggerEnter(Collider collider)
    {
        IInteractable interactable = collider.GetComponentInParent<IInteractable>();

        if (interactable != null)
        {
            interactable.Interact(gameObject);
        }
    }

    void IInteractable.Interact(GameObject source)
    {
        Debug.Log($"{source.name} interacted with {gameObject.name}")
    }
}
```

Please feel free to ask the lead programmer for more guidance about our [EDA] and how you can architecture your code when working with us.

---
# Unity Hybrid Entity Component System (ECS)
[ECS]:#unity-hybrid-entity-component-system-ecs

## Special naming
- Prefix `Shared` for any `ISharedComponentData`
- Suffix `Tag` for any **empty** `IComponentData`.
- Suffix `BufferElement` for any `IBufferElementData`.
- Suffix `System` for any `ComponentSystem`.
- Suffix `JobSystem` for any `JobComponentSystem`.
- Suffix `Authoring` for any authoring `MonoBehaviour`.
- For any IComponentData just use the data name itself (do **not** suffix anything).

## About
[ECS] still need more time to mature and receive some features that we already have in our `GameObject`/`MonoBehaviour` world, but that doesn't mean we can't use it yet. For specific parts of the code that we need some extra performance we encourage the use of the Hybrid [ECS].

Due the nature of our [EDA], there is no major barrier to cross between those two worlds safely. We can easily have high performance code running in [ECS] while still being able to use all the goodness that Unity developed for us since the very beginning.

In case of doubt you can always ask the lead programmer about it.
